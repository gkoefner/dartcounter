package com.gkgk.dartcounter;

/**
 * holds data about a single dart throw
 * Created by gordon on 1/9/15.
 */
public class HitData {

    public enum Field {
        MISS,
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE,
        SIX,
        SEVEN,
        EIGHT,
        NINE,
        TEN,
        ELEVEN,
        TWELVE,
        THIRTEEN,
        FOURTEEN,
        FIFTEEN,
        SIXTEEN,
        SEVENTEEN,
        EIGHTEEN,
        NINETEEN,
        TWENTY,
        OUTER_BULLS,
        BULLS
    }

    private Field field;

    //single, double triple (1,2,3)
    private int modifier;

    public HitData() {
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public int getModifier() {
        return modifier;
    }

    public void setModifier(int modifier) {
        this.modifier = modifier;
    }

    @Override
    public String toString() {
        return modifier + "x " + field;
    }
}
