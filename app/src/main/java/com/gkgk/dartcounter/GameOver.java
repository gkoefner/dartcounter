package com.gkgk.dartcounter;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class GameOver extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);

        Intent intent = getIntent();
        Player[] players = (Player[]) intent.getSerializableExtra(GameActivity.EXTRA_GAME);

        TableLayout scoreTable = (TableLayout) findViewById(R.id.score_table);

        for(Player p : players) {
            // Create the text view
//            TableRow row = new TableRow(this);
//            TextView text_p = new TextView(this);
//            TextView text_s = new TextView(this);
//            row.addView(text_p);
//            row.addView(text_s);
//            scoreTable.addView(row);

        }

        //String gametype = intent.getStringExtra(MainActivity.EXTRA_GAMETYPE);
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_game_over, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
