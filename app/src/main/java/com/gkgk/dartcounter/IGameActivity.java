package com.gkgk.dartcounter;

/**
 * Created by gordon on 1/12/15.
 */
public interface IGameActivity {
    public Game getCurrentGame();
    public void onDartThrown(HitData hit);
    public void onEndTurn();
    public void onBeginTurn();
    public void onEndRound();
    public void onEndGame();
}
