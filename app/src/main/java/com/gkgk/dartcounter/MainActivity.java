package com.gkgk.dartcounter;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.content.Intent;
import android.widget.EditText;


public class MainActivity extends ActionBarActivity {

    public final static String EXTRA_GAMETYPE = "com.gkgk.dartcounter.GAMETYPE";
    public final static String EXTRA_PLAYERS = "com.gkgk.dartcounter.PLAYERS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * called when the user clicks on a game type button
     * @param view
     */
    public void startGame(View view) {
        Intent intent = new Intent(this, GameActivity.class);

        EditText editPlayers = (EditText) findViewById(R.id.edit_players);

        Integer players = 2;
        if(!editPlayers.getText().toString().isEmpty()) {
            players = Integer.parseInt(editPlayers.getText().toString());
        }

        intent.putExtra(EXTRA_PLAYERS, players);

        switch(view.getId()) {
            case R.id.gametype401:
                intent.putExtra(EXTRA_GAMETYPE, "401");
                break;
            case R.id.gametype501:
                intent.putExtra(EXTRA_GAMETYPE, "501");
                break;
            case R.id.gametype301:
            default:
                intent.putExtra(EXTRA_GAMETYPE, "301");
                break;
        }

        startActivity(intent);
    }
}
