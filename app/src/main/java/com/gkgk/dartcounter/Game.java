package com.gkgk.dartcounter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * basic 301
 * Created by gordon on 1/9/15.
 */
public class Game implements Serializable {

    private List<Player> players;
    private List<Player> winners;
    private int round;
    private int currentPlayerIndex;
    private int dartsLeft;
    private boolean playerReachedZero;
    private List<HitData> hits;

    public final static int[] fieldValues = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 50};
    public final static int[] maxModifier = { 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1};

    private final static int DARTS = 3;
    private int maxRounds = -1;

    private IGameActivity frontend;

    public Game(IGameActivity frontend) {
        this.frontend = frontend;
        playerReachedZero = false;
    }

    public void initGame(List<Player> players, int startingScore, int maxRounds) {
        this.players = players;
        for(Player player : this.players) {
            player.setScore(startingScore); //starting score
        }

        hits = new ArrayList<>(3);

        currentPlayerIndex = 0;
        round = 1;
        this.maxRounds = maxRounds;

        beginTurn();
    }

    public void throwDart(HitData hit) {
        dartsLeft--;
        hits.add(hit);

        frontend.onDartThrown(hit);
        if(dartsLeft == 0) {
            endTurn();
        }
    }

    private void beginTurn() {
        dartsLeft = DARTS;
        hits.clear();
        frontend.onBeginTurn();
    }

    public void endTurn() {

        int roundScore = 0;
        for(HitData hit : hits) {
            roundScore += fieldValues[hit.getField().ordinal()] * (hit.getModifier() % (maxModifier[hit.getField().ordinal()]+1));
        }

        //apply round score
        Player currentPlayer = getCurrentPlayer();
        int score = currentPlayer.getScore() - roundScore;
        if(score >= 0) {
            currentPlayer.setScore(score);
            if(score == 0) {
                playerReachedZero = true;
            }
        }

        //next Player
        currentPlayerIndex = (currentPlayerIndex+1) % players.size();

        if(currentPlayerIndex == 0) {
            endRound();
        }
        beginTurn();
        frontend.onEndTurn();
    }

    public void endRound() {

        if(round == maxRounds || playerReachedZero) {
            endGame();
            return;
        }
        round++;
        frontend.onEndRound();
    }

    /**
     * called when someone wins
     */
    private void endGame() {
        //determine winners

        winners = new ArrayList<>();
        for(Player player : players) {
            winners.add(player);
        }

        frontend.onEndGame();
    }

    public List<Player> getWinners() {
        return winners;
    }

    public List<HitData> getHits() {
        return hits;
    }

    public int getDartsLeft() {
        return dartsLeft;
    }

    public Player getCurrentPlayer() {
        return players.get(currentPlayerIndex);
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }
}
