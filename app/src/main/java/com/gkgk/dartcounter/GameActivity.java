package com.gkgk.dartcounter;


import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import com.gkgk.dartcounter.fragments.NextPlayerDialogFragment;

import java.util.ArrayList;
import java.util.List;


public class GameActivity extends ActionBarActivity implements IGameActivity {

    public final static String EXTRA_GAME = "com.gkgk.dartcounter.CURRENTGAME";

    private Game currentGame;
    private int mModifier = 1;

    NextPlayerDialogFragment nextPlayerDialogFragment;
    LinearLayout currentTurnLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        Intent intent = getIntent();
        int n_players = intent.getIntExtra(MainActivity.EXTRA_PLAYERS, 2);
        String gametype = intent.getStringExtra(MainActivity.EXTRA_GAMETYPE);

        //setup display
        nextPlayerDialogFragment = new NextPlayerDialogFragment();      // Check that the activity is using the layout version with
        mModifier = 1;
        ((RadioButton) findViewById(R.id.radio_single)).setChecked(true);

        currentTurnLayout = (LinearLayout) findViewById(R.id.current_turn_info);

        //setup game

        currentGame = new Game(this);
        List<Player> players = new ArrayList<>(n_players);
        for(int i=0; i < n_players; i++) {
            Player newPlayer = new Player();
            newPlayer.setPlayerName("Player " + (i+1));
            players.add(newPlayer);
        }

        switch(gametype) {
            case "401":
                currentGame.initGame(players, 401, -1);
                break;
            case "501":
                currentGame.initGame(players, 501, -1);
                break;
            case "301":
            default:
                currentGame.initGame(players, 301, -1);
                break;
        }

        mModifier = 1;



        updateHUD();
    }

    private void displayNextTurnAlert() {

        nextPlayerDialogFragment.setPlayerName(currentGame.getCurrentPlayer().getPlayerName());
        nextPlayerDialogFragment.setPlayerScore(currentGame.getCurrentPlayer().getScore());
        nextPlayerDialogFragment.show(getSupportFragmentManager(), "next_turn_fragment");
    }
    public void setModifier(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        if (checked) {
            // Check which radio button was checked4
            switch (view.getId()) {
                case R.id.radio_single:
                    mModifier = 1;
                    break;
                case R.id.radio_double:
                    mModifier = 2;
                    break;
                case R.id.radio_triple:
                    mModifier = 3;
                    break;
            }
        }

    }

    public void throwDart(View view) {
        HitData hitData = new HitData();

        hitData.setModifier(mModifier);

        switch(view.getId()) {

            case R.id.button_hit_1:
                hitData.setField(HitData.Field.ONE);
                break;
            case R.id.button_hit_2:
                hitData.setField(HitData.Field.TWO);
                break;
            case R.id.button_hit_3:
                hitData.setField(HitData.Field.THREE);
                break;
            case R.id.button_hit_4:
                hitData.setField(HitData.Field.FOUR);
                break;
            case R.id.button_hit_5:
                hitData.setField(HitData.Field.FIVE);
                break;
            case R.id.button_hit_6:
                hitData.setField(HitData.Field.SIX);
                break;
            case R.id.button_hit_7:
                hitData.setField(HitData.Field.SEVEN);
                break;
            case R.id.button_hit_8:
                hitData.setField(HitData.Field.EIGHT);
                break;
            case R.id.button_hit_9:
                hitData.setField(HitData.Field.NINE);
                break;
            case R.id.button_hit_10:
                hitData.setField(HitData.Field.TEN);
                break;
            case R.id.button_hit_11:
                hitData.setField(HitData.Field.ELEVEN);
                break;
            case R.id.button_hit_12:
                hitData.setField(HitData.Field.TWELVE);
                break;
            case R.id.button_hit_13:
                hitData.setField(HitData.Field.THIRTEEN);
                break;
            case R.id.button_hit_14:
                hitData.setField(HitData.Field.FOURTEEN);
                break;
            case R.id.button_hit_15:
                hitData.setField(HitData.Field.FIFTEEN);
                break;
            case R.id.button_hit_16:
                hitData.setField(HitData.Field.SIXTEEN);
                break;
            case R.id.button_hit_17:
                hitData.setField(HitData.Field.SEVENTEEN);
                break;
            case R.id.button_hit_18:
                hitData.setField(HitData.Field.EIGHTEEN);
                break;
            case R.id.button_hit_19:
                hitData.setField(HitData.Field.NINETEEN);
                break;
            case R.id.button_hit_20:
                hitData.setField(HitData.Field.TWENTY);
                break;
            case R.id.button_hit_bulls:
                hitData.setField(HitData.Field.BULLS);
                break;
            case R.id.button_hit_outerbulls:
                hitData.setField(HitData.Field.OUTER_BULLS);
                break;

            case R.id.button_miss:
            default:
                hitData.setField(HitData.Field.MISS);
                break;

        }
        currentGame.throwDart(hitData);
    }

    public void endGame() {
        Intent intent = new Intent(this, GameOver.class);

        intent.putExtra(EXTRA_GAME, currentGame.getPlayers().toArray());

        startActivity(intent);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_game, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public Game getCurrentGame() {
        return currentGame;
    }

    @Override
    public void onDartThrown(HitData hit) {
        //TextView dartsLeft = (TextView) findViewById(R.id.textView_dartsleft);
       // dartsLeft.setText(getString(R.string.darts_left)+": " + currentGame.getDartsLeft());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
        TextView newText = new TextView(this);
        newText.setText(hit.toString());
        newText.setPadding(20,0,0,0);
        currentTurnLayout.addView(newText);


        //reset modifier to default value
        mModifier = 1;
        ((RadioButton) findViewById(R.id.radio_single)).setChecked(true);

        updateHUD();
    }

    private void updateHUD() {
//        Player currentPlayer = currentGame.getCurrentPlayer();
//        TextView playerName = (TextView) findViewById(R.id.textView_playername);
//        TextView score = (TextView) findViewById(R.id.textView_score);
//        playerName.setText(currentPlayer.getPlayerName());
//        score.setText(getString(R.string.score)+": "+currentPlayer.getScore());
//
//        TextView dartsLeft = (TextView) findViewById(R.id.textView_dartsleft);
//        dartsLeft.setText(getString(R.string.darts_left)+": "+currentGame.getDartsLeft());
//
//        LinearLayout layout = (LinearLayout) findViewById(R.id.current_turn_info);
//        for(HitData hit : currentGame.getHits()) {
//            TextView newThrow = new TextView(this);
//            newThrow.setText(hit.getField().toString());
//            layout.addView(newThrow);
//        }
    }

    @Override
    public void onBeginTurn() {

        Player currentPlayer = currentGame.getCurrentPlayer();

        currentTurnLayout.removeAllViewsInLayout();

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
        TextView newText = new TextView(this);
        newText.setText(currentPlayer.getPlayerName());
        newText.setPadding(20,0,0,0);
        currentTurnLayout.addView(newText);

        newText = new TextView(this);
        newText.setLayoutParams(params);
        newText.setText(getString(R.string.score)+": "+currentPlayer.getScore());
        newText.setPadding(20,0,0,0);
        currentTurnLayout.addView(newText);

    }

    @Override
    public void onEndTurn() {
       // updateHUD();
        displayNextTurnAlert();
    }

    @Override
    public void onEndRound() {
        updateHUD();
    }

    @Override
    public void onEndGame() {
        endGame();
    }
}
