package com.gkgk.dartcounter.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.gkgk.dartcounter.R;


/**
 * A simple  dialog fragment class used to display a dialog when it is the next players turn
 */
public class NextPlayerDialogFragment extends DialogFragment {

    String playerName = "";
    int playerScore;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(playerName + "'s " + getString(R.string.dialog_turn) +"   " + playerScore)
                .setPositiveButton(R.string.ok, null);
        // Create the AlertDialog object and return it
        return builder.create();
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getPlayerScore() {
        return playerScore;
    }

    public void setPlayerScore(int playerScore) {
        this.playerScore = playerScore;
    }
}
