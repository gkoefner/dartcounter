package com.gkgk.dartcounter;

import java.io.Serializable;

/**
 * basic class to hold player data
 * Created by gordon on 1/9/15.
 */
public class Player implements Serializable{
    private String playerName;
    private int score;

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
}
